# dp-course
> Laboratories for SEIS 664.

## Getting Started 
This repository contains labs supporting the Digital Practitioner Body of Knowledge standard delivered as a semester-long, Masters' level course. 

There is at this writing one technical lab a week. There is also a summary, multiple-choice, final at the end of the semester.

It contains an assortment of text files and some binaries (small images).

## License

> This project is licensed under the Creative Commons Attribution-NonCommercial 4.0 International Public License.

** See the [LICENSE](@LICENSE)© file for details.
 
